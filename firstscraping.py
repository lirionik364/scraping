from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError


def getTitle(url):
	try:
		html = urlopen(url)
	except HTTPError as e:
		return None
	try:
		bsObj = BeautifulSoup(html.read())
		title = bsObj.body.h1
	except AttributteError as e:
		return None
	return title

title = getTitle("http://pythonscraping.com/pages/page1.html")
if title == None:
	print("Title could not be found")
else:
	print(title)
